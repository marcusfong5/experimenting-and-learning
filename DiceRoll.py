import random

Dice1 = [
    1, 2, 3, 4, 5, 6
]

sumOne = 0
sumTwo = 0
sumThree = 0
sumFour = 0
sumFive = 0
sumSix = 0


def trial(rolls):
    global sumOne, sumTwo, sumThree, sumFour, sumFive, sumSix
    if int(rolls) < 0:
        return null
    while int(rolls) >= 1:
        y = random.choice(Dice1)
        if y == 1:
            sumOne += 1
        elif y == 2:
            sumTwo += 1
        elif y == 3:
            sumThree += 1
        elif y == 4:
            sumFour += 1
        elif y == 5:
            sumFive += 1
        elif y == 6:
            sumSix += 1
        rolls -= 1



print("--Dice Roll Simulator--")


# print()
# r1 = input('Enter number of rolls: ')
# trial(int(r1))
# print("One: " + str(sumOne))
# print("Two: " + str(sumTwo))
# print("Three: " + str(sumThree))
# print("Four: " + str(sumFour))
# print("Five: " + str(sumFive))
# print("Six: " + str(sumSix))
# print()

def numberOfTrials(r1):
    trial(int(r1))
    print("One: " + str(sumOne))
    print("Two: " + str(sumTwo))
    print("Three: " + str(sumThree))
    print("Four: " + str(sumFour))
    print("Five: " + str(sumFive))
    print("Six: " + str(sumSix))
    print()


while True:
    # main program
    x = input('Enter number of rolls: ')
    numberOfTrials(x)
    if True:
        continue

