# Import pygame library
import pygame
import random
import Player

# Import pygame locals for access to keys
from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    KEYDOWN,
    K_ESCAPE,
    QUIT,
)


class Player(pygame.sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__()
        # Surface drawn on screen now attribute of player

        # Define size
        self.surf = pygame.Surface((75, 75))

        # Define colour (R, G, B, Alpha)
        self.surf.fill((255, 255, 255, 255))
        self.rect = self.surf.get_rect()

    # Create movement of Player
    def update(self, pressed_keys):
        if pressed_keys[K_UP]:
            self.rect.move_ip(0, -5)
        if pressed_keys[K_DOWN]:
            self.rect.move_ip(0, 5)
        if pressed_keys[K_LEFT]:
            self.rect.move_ip(-5, 0)
        if pressed_keys[K_RIGHT]:
            self.rect.move_ip(5, 0)

        # Keep player on the screen
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= 0:
            self.rect.top = 0
        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT


class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        super(Enemy, self).__init__()

        # Set size of enemy
        self.surf = pygame.Surface((20, 10))

        # Set colour of enemy
        self.surf.fill((0, 0, 255, 255))

        # Set position of enemy
        self.rect = self.surf.get_rect(
            center=(
                random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100),
                random.randint(0, SCREEN_HEIGHT),
            )
        )
        self.speed = random.randint(5, 20)

    # Move sprite based on speed
    # Remove the sprite when it passes the left edge of screen
    def update(self):
        self.rect.move_ip(-self.speed, 0)
        if self.rect.right < 0:
            self.kill()


# Initialize pygame library
pygame.init()

# Set up drawing window
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])

# Create custom add enemy event
# USEREVENT denotes last event pygame reserves
ADDENEMY = pygame.USEREVENT + 1
# calls ADDENEMY per 250 milliseconds
pygame.time.set_timer(ADDENEMY, 250)

# Instantiate player
player = Player()

# Create groups to hold enemy sprites and all sprites
# enemy for collision detection and position updates
# all_sprites for rendering
enemies = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
all_sprites.add(player)

# Game Loop
running = True
while running:

    # Check every event in event list per frame
    for event in pygame.event.get():

        # Check if user hits a key
        if event.type == KEYDOWN:
            # Check if key was ESC
            if event.key == K_ESCAPE:
                running = False

        # Check if user clicks close window
        elif event.type == QUIT:
            running = False

        # Add new enemy
        elif event.type == ADDENEMY:
            # Create the new enemy and add it to sprite groups
            new_enemy = Enemy()
            enemies.add(new_enemy)
            all_sprites.add(new_enemy)

    # Get list of keys pressed per frame
    press_keys = pygame.key.get_pressed()

    # Update movement after key presses
    player.update(press_keys)
    #
    # Update enemy position
    enemies.update()

    # Fill background white
    # fill(colour) :: fill(int R, int G, int B)
    screen.fill((0, 0, 0))

    # blit :: layers surf on top of screen
    # screen.blit(player.surf, (SCREEN_WIDTH/2, SCREEN_HEIGHT/2))

    # players surf on top left corner
    for entity in all_sprites:
        screen.blit(entity.surf, entity.rect)

    if pygame.sprite.spritecollideany(player, enemies):
        # If so, then remove the player and stop the loop
        player.kill()
        running = False

    # Push contents onto the display
    pygame.display.flip()

# Quit
pygame.quit()
